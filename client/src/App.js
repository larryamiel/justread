import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';

import Header from './components/layout/Header';
import Footer from './components/layout/Footer';

import Home from './components/pages/home/Home';
import Login from './components/pages/auth/login/Login';
import Logout from './components/pages/auth/logout/Logout';
import Register from './components/pages/auth/register/Register';
import Manga from './components/pages/manga/Manga';
import Reader from './components/pages/reader/Reader';
import UserHistory from './components/pages/user/history/UserHistory';
import UserFavorites from './components/pages/user/list/UserFavorites';

import { AuthRoute } from './route-types/AuthRoute';
import { GuestRoute } from './route-types/GuestRoute';

function App() {
  return (
    <Router>
      <div className="app">
        <Header />
          <div className="app-container">
            <AuthRoute path="/" exact component={Home} />
            <GuestRoute path="/login" component={Login} />
            <GuestRoute path="/register" component={Register} />
            <AuthRoute exact path="/user/logout" component={Logout} />
            <AuthRoute exact path="/user/history" component={UserHistory} />
            <AuthRoute exact path="/user/favorites" component={UserFavorites} />
            <AuthRoute exact path="/manga/:id" component={Manga} />
            <AuthRoute exact path="/manga/:id/:chapter/:page?" component={Reader} />
          </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
