class Auth {
    login = (user) => {
        localStorage.setItem('userData', JSON.stringify(user));
    }

    logout = () => {
        localStorage.removeItem('userData');
    }

    user = () => {
        if (localStorage.getItem('userData')) {
            return JSON.parse(localStorage.getItem('userData'));
        }
        else {
            return null;
        }        
    }
}

export default new Auth();