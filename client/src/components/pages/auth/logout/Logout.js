import axios from 'axios';
import React, { useEffect } from 'react';
import Auth from '../../../../Auth';

function Logout(props) {
    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/user/logout', {}, {withCredentials: true})
            .then(res => {
                console.log(res);
                
                Auth.logout();
                props.history.push('/login');
            }).catch(err => console.log(err));
    });

    return (<></>);
}

export default Logout;