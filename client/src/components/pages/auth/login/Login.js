import {React, useState } from 'react';
import axios from 'axios';
import './Login.css';
import Auth from '../../../../Auth';
import Banner from '../../../layout/Banner';

function Login(props) {
    const [username, setUsername] = useState(() => {return '';});
    const [password, setPassword] = useState(() => {return '';});
    const [alerts, setAlerts] = useState(() => {return [];});

    const login = (e) => {
        e.preventDefault();
        
        axios.post(process.env.REACT_APP_API_URL + "/user/login", {
            username: username,
            password: password
        }, { withCredentials: true })
        .then(response => {
            console.log(response);

            if ( response.data.error ) {
                displayErrors(response.data.error);
            }
            else {
                // Login
                Auth.login(response.data.user);
                props.history.push('/');
            }
        }).catch(err => console.log(err));
    }

    const displayErrors = (err) => {
        setAlerts(alerts => alerts = err);

        setTimeout(() => {
            setAlerts(alerts => alerts = []);
        }, 5000);
    }

    return (
        <div id="page-login">
            <Banner text="Login" />

            <div className="form-container login">
                <form onSubmit={login}>
                    {alerts.map((alert, index) => {
                        return <p key={index} className="form-alert">{alert}</p>;
                    })}

                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-user"></i> { username === '' ? 'Username' : null }</label>
                        <input className="form-control" type="text" id="username" value={username} onChange={e => setUsername(e.target.value)} name="username"/>
                    </div>
                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-key"></i> { password === '' ? 'Password' : null }</label>
                        <input className="form-control" type="password" id="password" value={password} onChange={e => setPassword(e.target.value)} name="password"/>
                    </div>
                    <div className="form-group">
                        <button id="submit" type="submit">Login</button>
                    </div>

                    <div className="registration">
                        <p>Don't have an account? <a className="register" href="/register">Register</a>.</p>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Login;