import {React, useState } from 'react';
import axios from 'axios';
import './Register.css';

import Banner from '../../../layout/Banner';

function Register(props) {
    const [email, setEmail] = useState(() => {return '';});
    const [username, setUsername] = useState(() => {return '';});
    const [password, setPassword] = useState(() => {return '';});
    const [confirmPassword, setConfirmPassword] = useState(() => {return '';});
    const [alerts, setAlerts] = useState(() => {return [];});

    const register = (e) => {
        e.preventDefault();

        axios.post(process.env.REACT_APP_API_URL + "/user/register", {
            email: email,
            username: username,
            password: password,
            confirmPassword: confirmPassword
        }, { withCredentials: true })
        .then(response => {
            console.log(response);
            if ( response.data.error ) {
                displayErrors(response.data.error);
            }
            else {
                success(response.data.msg);
            }
        }).catch(err => console.log(err));
    }

    const displayErrors = (err) => {
        setAlerts(alerts => alerts = err);

        setTimeout(() => {
            setAlerts(alerts => alerts = []);
        }, 5000);
    }

    const success = (msg) => {
        // Show Success
        setAlerts(alerts => alerts = [msg]);

        // Redirect after 1 second
        setTimeout(() => {
            props.history.push('/login');
        }, 2000);
    }

    return (
        <div id="page-register">
            <Banner text="Register" />

            <div className="form-container register">
                <form onSubmit={register}>
                    {alerts.map((alert, index) => {
                        return <p key={index} className="form-alert">{alert}</p>;
                    })}

                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-envelope"></i> { email === '' ? 'Email' : null }</label>
                        <input className="form-control" type="email" id="email" onChange={e => setEmail(e.target.value)} value={email} name="email"/>
                    </div>
                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-user"></i> { username === '' ? 'Username' : null }</label>
                        <input className="form-control" type="text" id="username" onChange={e => setUsername(e.target.value)}  value={username} name="username"/>
                    </div>
                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-key"></i> { password === '' ? 'Password' : null }</label>
                        <input className="form-control" type="password" id="password" onChange={e => setPassword(e.target.value)} value={password} name="password"/>
                    </div>
                    <div className="form-group">
                        <label className="form-label"><i className="fa fa-key"></i> { confirmPassword === '' ? 'Confirm Password' : null }</label>
                        <input className="form-control" type="password" id="confirm-password" onChange={e => setConfirmPassword(e.target.value)} value={confirmPassword} name="confirm-password"/>
                    </div>
                    <div className="form-group">
                        <button id="submit" type="submit">Register</button>
                    </div>

                    <div className="login">
                        <p>Already have an account? <a className="login" href="/login">Login</a>.</p>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Register;