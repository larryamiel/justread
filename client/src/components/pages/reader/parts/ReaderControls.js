import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import './ReaderControls.css';

function ReaderControls(props) {
    const [showControls, setShowControls] = useState(true);
    
    useEffect(() => {
        window.addEventListener('scroll', (e) => {
            const windowHeight = window.innerHeight;
            const documentHeight = window.document.body.offsetHeight;

            if (window.pageYOffset <= 200 || window.pageYOffset >= (documentHeight - windowHeight - 100)) {
                setShowControls(true);
            }
            else {
                setShowControls(false);
            }
        });
    }, []);

    return (
        <div className={showControls ? 'reader-controls show' : 'reader-controls'}>
            <div className="controls">
                <div className="chapter-select">
                    <select onChange={event => props.changeChapter(event.target.value)} value={props.chapter.index} name="chapter-controls" id="chapter-controls">
                        {props.mangaChapters.map(mangaChapter => {
                            return (
                            <option key={mangaChapter.index} value={mangaChapter.index}>
                                {mangaChapter.title}
                            </option>
                            );
                        })}
                    </select>
                </div>

                <div className="chapter-next-prev">
                    { props.chapter.index > 1 ? 
                    <div onClick={event => props.prevChapter()} className="chapter-control">
                        <i className="fas fa-arrow-circle-left" title="Previous Chapter"></i>
                    </div> : null }
                    { props.chapter.index < props.mangaChapters.length ? 
                    <div onClick={event => props.nextChapter()} className="chapter-control">
                        <i className="fas fa-arrow-circle-right" title="Next Chapter"></i>
                    </div> : null }
                </div>
            </div>
            <div className="controls">
                <p className="chapter-pages">{props.page} of {props.pageCount}</p>
            </div>
            <div className="controls">
                <div className="read-mode-options">
                    <span>Read Mode</span>

                    <div onClick={event => props.changeReadMode('page')} className={props.readMode === 'page' ? 'read-mode-option current' : 'read-mode-option'}>
                        <i className="fas fa-arrows-alt-h" title="Page Mode"></i>
                    </div>
                    <div onClick={event => props.changeReadMode('strip')} className={props.readMode === 'strip' ? 'read-mode-option current' : 'read-mode-option'}>
                        <i className="fas fa-arrows-alt-v" title="Strip Mode"></i>
                    </div>
                </div>

                <div className="reader-close">
                    <Link to={'/manga/' + props.mangaID}><i className="fas fa-times"></i></Link>
                </div>
            </div>
        </div>
    );
}

export default ReaderControls;