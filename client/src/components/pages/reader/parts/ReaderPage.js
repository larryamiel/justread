import React from 'react';
import './ReaderPage.css'

function ReaderPage(props) {
    const getPages = (mangaID, chapter, pageCount) => {
        let content = [];

        for ( let i = 1; i <= pageCount; i++ ) {
            content.push(
                <div key={'page-' + i} className={props.page === i ? 'reader-page current' : 'reader-page'}>
                    <img alt={chapter.title + ' page ' + i}
                        src={process.env.REACT_APP_API_URL + '/storage/manga/' + mangaID + '/' + chapter.index + '/' + i + '.jpg'} />
                </div>
            )
        }

        return content;
    }

    return (
        <div className="reader-page-container">
            <div className="controllers">
                <div className="prev" onClick={props.prevPage}></div>
                <div className="next" onClick={props.nextPage}></div>
            </div>

            {getPages(props.mangaID, props.chapter, props.pageCount)}
        </div>
    );
}

export default ReaderPage;