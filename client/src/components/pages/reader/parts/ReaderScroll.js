import React, { useEffect, useRef } from 'react';
import ScrollTrigger from 'react-scroll-trigger';
import './ReaderScroll.css';

function ReaderScroll(props) {
    const currentPage = useRef(null);

    useEffect(() => {
        if ( currentPage.current ) {
            currentPage.current.scrollIntoView();
        }
    }, [props.readMode]);

    const getPages = (mangaID, chapter, pageCount) => {
        let content = [];

        for ( let i = 1; i <= pageCount; i++ ) {
            if (props.page === i) {
                content.push(
                    <ScrollTrigger triggerOnLoad={false} onEnter={() => {props.changePage(i)}} key={'page-' + i} className="reader-scroll">
                        <img ref={currentPage} alt={chapter.title + ' page ' + i}
                            src={process.env.REACT_APP_API_URL + '/storage/manga/' + mangaID + '/' + chapter.index + '/' + i + '.jpg'} />
                    </ScrollTrigger>
                )
            }
            else {
                content.push(
                    <ScrollTrigger triggerOnLoad={false} onEnter={() => {props.changePage(i)}} key={'page-' + i} className="reader-scroll">
                        <img alt={chapter.title + ' page ' + i}
                            src={process.env.REACT_APP_API_URL + '/storage/manga/' + mangaID + '/' + chapter.index + '/' + i + '.jpg'} />
                    </ScrollTrigger>
                )
            }
        }

        return content;
    }

    return (
        <div className="reader-scroll-container">
            {getPages(props.mangaID, props.chapter, props.pageCount)}
        </div>
    );
}

export default ReaderScroll;