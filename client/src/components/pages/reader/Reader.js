import React, { useEffect, useState } from 'react';
import ReaderControls from './parts/ReaderControls';
import ReaderPage from './parts/ReaderPage';
import axios from 'axios';

import Auth from './../../../Auth';

import './Reader.css';
import ReaderScroll from './parts/ReaderScroll';

function Reader(props) {
    const [mangaChapters, setMangaChapters] = useState([]);
    const [readMode, setReadMode] = useState('page');
    const [chapter, setChapter] = useState({});
    const [page, setPage] = useState(1);
    const [pageCount, setPageCount] = useState(0);

    // Add a View
    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/manga/' + props.match.params.id + '/' + props.match.params.chapter + '/view', {}, {withCredentials: true})
            .then(res => {
                const data = res.data;
                console.log(data);
            }).catch(err => console.log(err));
    }, [props.match.params.id, props.match.params.chapter]);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/manga/' + props.match.params.id + '/' + props.match.params.chapter, {}, {withCredentials: true})
            .then((res) => {
                // Set Manga
                console.log(res.data);

                setChapter(chapter => {
                    return chapter = res.data;
                });

                setPageCount(pageCount => {
                    return pageCount = res.data.page_count;
                });
                
                setPage(page => {
                    if ( page === -1 ) {
                        return page = res.data.page_count;
                    }
                    else if ( page === -2 || page === 1 ) {
                        return page = 1;
                    }
                    else {
                        return page;
                    }
                });
            }).catch(err => console.log(err));
    }, [props.match.params.id, props.match.params.chapter]);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/manga/' + props.match.params.id, {}, {withCredentials: true})
            .then(res => {
                // Get Chapters
                setMangaChapters(mangaChapters => {
                    return mangaChapters = res.data.chapters;
                });
            })
    }, [props.match.params.id]);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/user/updateHistory', {
            username: Auth.user().username,
            mangaID: props.match.params.id,
            chapter: props.match.params.chapter,
            page: page
        }, {withCredentials: true})
            .then((res) => {
                console.log(res.data);
            }).catch(err => console.log(err));
    }, [props.match.params.id, props.match.params.chapter, page]);

    useEffect(() => {
        // Set Page when there is param page
        if ( props.match.params.page ) {
            setPage(page => page = +props.match.params.page);
        }
    }, [props.match.params.page]);

    const changeChapter = (chapterIndex) => {
        setPage(page => page = 1);
        setPageCount(pageCount => pageCount = 0);
        props.history.push('/manga/' + props.match.params.id.toString() + '/' + chapterIndex.toString());
    }

    const nextChapter = () => {
        setPage(page => page = 1);
        setPageCount(pageCount => pageCount = 0);
        props.history.push('/manga/' + props.match.params.id.toString() + '/' + (+props.match.params.chapter + 1).toString());
    }

    const prevChapter = () => {
        setPage(page => page = 1);
        setPageCount(pageCount => pageCount = 0);
        props.history.push('/manga/' + props.match.params.id.toString() + '/' + (+props.match.params.chapter - 1).toString());
    }

    const nextPage = () => {
        setPage(page => {
            if ( (page + 1) > pageCount) {
                if ( +chapter.index !== +mangaChapters.length ) {
                    return page = -2;
                }
                else {
                    return page;
                }
            }
            else {
                return page += 1;
            }
        });

        if ( (page + 1) > pageCount ) {
            if ( +chapter.index !== +mangaChapters.length ) {
                setPageCount(pageCount => {
                    return pageCount = 0;
                });
                props.history.push('/manga/' + props.match.params.id.toString() + '/' + (chapter.index + 1).toString());
            }
        }
    }

    const prevPage = () => {
        setPage(page => {
            if ( page - 1 === 0 ) {
                return page = 1;
            }
            else {
                return page -= 1;
            }
        });

        if ( page - 1 === 0 ) {
            if ( chapter.index > 1 ) {
                setPage(page => {
                    return page = -1;
                });
                setPageCount(pageCount => {
                    return pageCount = 0;
                });
                props.history.push('/manga/' + props.match.params.id.toString() + '/' + (chapter.index - 1).toString());
            }
        }
    }

    const changePage = (newPage) => {
        setPage(page => newPage);
    }

    const changeReadMode = (mode) => {
        setReadMode(readMode => mode);
    }

    return (
        <div className="reader">
            <ReaderControls 
                chapter={chapter} pageCount={pageCount} mangaID={props.match.params.id} changeReadMode={changeReadMode} readMode={readMode}
                page={page} mangaChapters={mangaChapters} changeChapter={changeChapter} nextChapter={nextChapter} prevChapter={prevChapter} />
            <div className="reader-container">
                {readMode === 'page' ? <ReaderPage mangaID={props.match.params.id} page={page}
                    chapter={chapter} pageCount={pageCount} nextPage={nextPage} prevPage={prevPage} /> : ''}
                {readMode === 'strip' ? <ReaderScroll mangaID={props.match.params.id} page={page} readMode={readMode}
                    chapter={chapter} pageCount={pageCount} nextPage={nextPage} prevPage={prevPage} changePage={changePage} /> : ''}
            </div>
        </div>
    );
}

export default Reader;