import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Auth from '../../../../Auth';
import MangaThumb from '../../../manga/manga-thumb/MangaThumb';
import Banner from '../../../layout/Banner';

import './UserHistory.css';

function UserHistory(props) {
    const [history, setHistory] = useState([]);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/user/history', { email: Auth.user().email }, {withCredentials: true})
            .then((res) => {
                setHistory((history) => {
                    let data = res.data;

                    for(let i in data) {
                        data[i].manga = {
                            title: data[i].manga_title,
                            index: data[i].mangaID
                        }
                    }

                    console.log(data);

                    return data;
                });
            }).catch(err => console.log(err));
    }, []);

    return (
        <>
            <Banner text="History"></Banner>
            <div className="user-history">
                { history.map((h) => {
                    return <MangaThumb key={h.mangaID} mangaID={h.mangaID} history={h} />
                }) }
            </div>
        </>
    );
}

export default UserHistory;