import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MangaThumb from '../../../manga/manga-thumb/MangaThumb';
import Auth from '../../../../Auth';
import './UserFavorites.css';
import Banner from '../../../layout/Banner';

function UserFavorites(props) {
    const [favorites, setFavorites] = useState([]);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/user/favorites', { email: Auth.user().email }, {withCredentials: true})
            .then(res => {
                setFavorites(favorites => {
                    return favorites = res.data;
                });
            }).catch(err => console.log(err));
    }, []);

    return (
        <>
            <Banner text="Your List" />
            <div className="user-favorites">
                { favorites.map((favorite) => {
                    return <MangaThumb key={favorite} mangaID={favorite} />
                }) }
            </div>
        </>
    );
}

export default UserFavorites;