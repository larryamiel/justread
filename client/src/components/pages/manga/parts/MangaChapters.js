import React from 'react';
import { Link } from 'react-router-dom';
import './MangaChapters.css';

import Loader from 'react-loader-spinner';

function Manga(props) {

    const Chapters = () => {
        if ( props.manga.chapters ) {
            return (
                props.manga.chapters.map((chapter) => {
                    return(
                        <tr key={chapter.index} className="manga-chapter">
                            <td>
                                <p className="manga-chapter"><Link to={'/manga/' + props.manga.index + '/' + chapter.index}>{chapter.title}</Link></p>
                            </td>
                            <td>
                                <p className="manga-views">{chapter.views ?? 0} &nbsp; &nbsp;<i className="fa fa-eye"></i></p>
                            </td>
                        </tr>
                    );
                })
            );
        }
        else {
            return (<div className="loader-container">
                        <Loader type="Circles" color="#05386b" height={30} width={30} />
                    </div>);
        }
    }

    return (
        <div className="manga-chapters">
            <h4>Chapters</h4>
            <table className="manga-chapter-list">
                <thead>
                    <tr>
                        <th width="600">Chapter #</th>
                        <th width="100">Views</th>
                    </tr>
                </thead>
                <tbody>
                    <Chapters />
                </tbody>
            </table>
        </div>
    );
}

export default Manga;