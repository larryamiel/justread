import React, {useEffect, useState} from 'react';
import Loader from 'react-loader-spinner';
import axios from 'axios';
import Auth from '../../../../Auth';
import MangaFavorites from './MangaFavorites';

import './MangaBanner.css';


function MangaBanner(props) {
    const [isFavorite, setIsFavorite] = useState(false);

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/user/favorites', {email: Auth.user().email}, {withCredentials: true})
            .then(res => {
                const data = res.data;

                console.log(data);

                if ( data.includes(props.manga.index) ) {
                    setIsFavorite(true);
                }
                else {
                    setIsFavorite(false);
                }
            }).catch(err => console.log(err));
    }, [props.manga.index]);

    const toggleAddToFavorites = () => {
        axios.post(process.env.REACT_APP_API_URL + '/user/toggleFavorite', {email: Auth.user().email, manga_index: props.manga.index}, {withCredentials: true})
            .then(res => {
                console.log(res);
                setIsFavorite(isFavorite => {
                    return !isFavorite;
                })
            }).catch(err => console.log(err));
    }

    return (
        <div className="manga-banner">
            <div className="manga-info">
                <div className="manga-thumbnail">
                    {
                        props.manga.index ? 
                        <img src={props.manga.index ? process.env.REACT_APP_API_URL + '/storage/images/' + props.manga.index + '.jpg': ''} alt={props.manga.title} />
                        :
                        <div className="loader-container">
                            <Loader type="Circles" color="#05386b" height={30} width={30} />
                        </div>
                    }
                </div>
                <div className="manga-text">
                    <h3 className="manga-title">{props.manga.title ?? <Loader type="Circles" color="#edf5e1" height={30} width={30} />}</h3>
                    <h4 className="manga-author">Written by: {props.manga.author}</h4>
                    <MangaFavorites isFavorite={isFavorite} toggleAddToFavorites={toggleAddToFavorites} />
                </div>
            </div>
        </div>
    );
}

export default MangaBanner;