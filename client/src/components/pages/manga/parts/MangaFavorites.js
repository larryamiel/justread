import React from 'react';
import './MangaFavorites.css';

function MangaAddToList(props) {
    return (
        <span className="add-to-favorites" onClick={props.toggleAddToFavorites}>
            <i className={props.isFavorite ? 'fas fa-heart' : 'far fa-heart'}></i> <span>{props.isFavorite ? 'Remove from Favorites' : 'Add to Favorites'}</span>
        </span>
    );
}

export default MangaAddToList;