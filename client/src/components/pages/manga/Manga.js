import {React, useEffect, useState} from 'react';
import MangaChapters from './parts/MangaChapters';
import MangaBanner from './parts/MangaBanner';
import axios from 'axios';

import './Manga.css';

function Manga(props) {
    const [manga, setManga] = useState({});

    useEffect(() => {
        axios.post(process.env.REACT_APP_API_URL + '/manga/' + props.match.params.id, {}, {withCredentials: true})
            .then((res) => {
                // Set Manga
                setManga(manga => manga = res.data);
            }).catch(err => console.log(err));
    }, [props.match.params.id]);

    return (
        <div className="manga-page">
            <MangaBanner manga={manga} />
            <div className="manga-synopsis">
                <p>{manga.description}</p>
            </div>
            <MangaChapters manga={manga}></MangaChapters>
        </div>
    );
}

export default Manga;