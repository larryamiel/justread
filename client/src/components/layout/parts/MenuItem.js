import React from 'react';
import { Link } from 'react-router-dom';

function MenuItem(props) {
    return (
        <li className="menu-item"><Link to={props.to}><i className={'fas fa-' + props.icon}></i> {props.label}</Link> {props.children}</li>
    );
}

export default MenuItem;