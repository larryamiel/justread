import React, { useState } from 'react';
import Auth from '../../../Auth';
import MenuItem from './MenuItem';
import './Sidebar.css';

function Sidebar(props) {
    const [toggle, setToggle] = useState(false);

    const toggleSidebar = () => {
        setToggle(!toggle);
    }

    return (
        <div className="sidebar-nav">
            <div className="sidebar-toggle-container">
                <span onClick={toggleSidebar} className="sidebar-toggle">{toggle ? <i className="fas fa-times"></i> : <i className="fas fa-bars"></i>}</span>
            </div>

            <div className={toggle ? 'sidebar open': 'sidebar'}>
                { Auth.user() ? 
                <ul onClick={toggleSidebar} className="main-menu">
                    <MenuItem to="/" icon="compass" label="Discover"></MenuItem>
                    <p className="divider">Account</p>
                    <MenuItem to="/user/history" icon="book" label="History"></MenuItem>
                    <MenuItem to="/user/favorites" icon="pencil-alt" label="Your List"></MenuItem>
                    <MenuItem to="/user/logout" icon="sign-out-alt" label="Logout"></MenuItem>
                </ul> :
                <ul onClick={toggleSidebar} className="main-menu">
                    <MenuItem to="/login" icon="user" label="Login"></MenuItem>
                    <MenuItem to="/register" icon="user" label="Register"></MenuItem>
                </ul> }
            </div>
        </div>
    );
}

export default Sidebar;