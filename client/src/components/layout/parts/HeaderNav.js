import React from 'react';
import Auth from '../../../Auth';
import MenuItem from './MenuItem';
import './HeaderNav.css';

function HeaderNav(props) {
    return (
        <div className="header-nav">
            { Auth.user() ? 
            <ul className="main-menu">
                <MenuItem to="/" icon="compass" label="Discover"></MenuItem>
                <MenuItem to="/user/favorites" icon="user" label={Auth.user() ? Auth.user().username: ''}>
                    <i className="fa fa-caret-down"></i>
                    <ul className="profile-sub-menu">
                        <MenuItem to="/user/history" icon="book" label="History"></MenuItem>
                        <MenuItem to="/user/favorites" icon="pencil-alt" label="Your List"></MenuItem>
                        <MenuItem to="/user/logout" icon="sign-out-alt" label="Logout"></MenuItem>
                    </ul>
                </MenuItem>
            </ul> :
            <ul className="main-menu">
                <MenuItem to="/login" icon="user" label="Login"></MenuItem>
                <MenuItem to="/register" icon="user" label="Register"></MenuItem>
            </ul> }
        </div>
    );
}

export default HeaderNav;