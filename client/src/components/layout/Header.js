import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Auth from '../../Auth';
import './Header.css';

import { useHistory } from 'react-router-dom' 
import HeaderNav from './parts/HeaderNav';
import Sidebar from './parts/Sidebar';

function Header() {
    const [user, setUser] = useState(Auth.user());
    const history = useHistory() 

    useEffect(() => {
        return history.listen(location => {
            setUser(Auth.user());
        });
    }, [user, history]);

    return (
        <div className="main-header">
            <div className="container">
                <Link to="/">
                    <div className="logo-container">
                        <img src="/logo-honya-600.jpg" alt="Honya" />
                    </div>
                </Link>
                <HeaderNav />
                <Sidebar />
            </div>
        </div>
    );
}

export default Header;