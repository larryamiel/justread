import axios from 'axios';
import { useEffect, useState } from 'react';
import Banner from '../layout/Banner';
import MangaThumb from './manga-thumb/MangaThumb';
import './MangaList.css';

function MangaList() {
  const [mangas, setMangas] = useState([]);

  useEffect(() => {
    axios.post(process.env.REACT_APP_API_URL + '/manga/all', {}, {withCredentials: true})
      .then(res => {
        setMangas(mangas => mangas = res.data);
      }).catch(err => console.log(err));
  }, []);

  return (
    <>
      <Banner text="Manga List"></Banner>
      <div id="component-manga-list">
        {mangas.map(manga => {
          return <MangaThumb key={manga.index} manga={manga} />
        })}
      </div>
    </>
  );
}

export default MangaList;