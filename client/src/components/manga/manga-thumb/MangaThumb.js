import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './MangaThumb.css';

import Loader from 'react-loader-spinner'

function MangaThumb(props) {
    const [manga, setManga] = useState({});

    const History = () => {
        return (
            <div className="history-float">
                <p>{'Chapter ' + props.history.chapter + ', Page ' + props.history.page}</p>
            </div>
        )
    }

    useEffect(() => {
        if ( props.manga ) {
            setManga(manga => {
                return manga = props.manga;
            });
        }
        else {
            axios.post(process.env.REACT_APP_API_URL + '/manga/' + props.mangaID, {}, {withCredentials: true})
                .then(res => {
                    setTimeout(() => {
                        setManga(manga => {
                            return manga = res.data;
                        })
                    }, 300);
                }).catch(err => console.log(err));
        }
    }, [props.manga, props.mangaID]);

    return (
        <div className="manga-thumb">
            { Object.keys(manga).length > 0 ?
                <Link to={props.history ? '/manga/' + props.history.mangaID + '/' + props.history.chapter + '/' + props.history.page : '/manga/' + manga.index}>
                    <div className="image-container">
                        <img src={manga.index ? process.env.REACT_APP_API_URL + '/storage/images/' + manga.index + '.jpg' : null} alt={manga.title} />
                    </div>
                    <div className="info-container">
                        <p className="manga-title">{manga.title}</p>
                    </div>
                    { props.history ? <History /> : null }
                </Link>
                :
                <div className="loader-container">
                    <Loader type="Circles" color="#05386b" height={30} width={30} />
                </div>
            }
        </div>
    );
}

export default MangaThumb;