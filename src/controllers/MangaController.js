const Manga = require('../models/Manga');
const fs = require('fs');
const download = require('download');

class MangaController {
    // Get All Manga
    all = (req, res) => {
        Manga.find()
            .select('-chapters')
            .then(mangas => {
                res.json(mangas);
            })
            .catch(err => console.log(err));
    }

    // Add Manga
    add = (req, res) => {
        const data = req.body;

        Manga.findOne({scrape_url: data.scrape_url})
            .then(async manga => {
                if (manga) {
                    fs.writeFileSync(__dirname + '/../storage/images/' + manga.index.toString() + '.jpg',
                            await download(data.thumbnail_url).catch(err => { console.log('Error: ' + err) }));

                    if (data.title) {
                        manga.title = data.title
                    }

                    if (data.description) {
                        manga.description = data.description
                    }

                    manga.save()
                            .then(() => {
                                res.json({
                                    status: 'success',
                                    msg: 'Manga has been successfully updated.',
                                });
                            }).catch(err => console.log(err));
                }
                else {
                    Manga.estimatedDocumentCount({}, async (err, count) => {
                        if (err) throw err;

                        const manga_index = 10000 + (+count + 1);

                        fs.writeFileSync(__dirname + '/../storage/images/' + manga_index.toString() + '.jpg',
                            await download(data.thumbnail_url).catch(err => { console.log('Error: ' + err) }));

                        const manga = new Manga({
                            index: manga_index,
                            title: data.title,
                            description: data.description,
                            scrape_url: data.scrape_url,
                            date_created: Date.now()
                        });

                        manga.save()
                            .then(() => {
                                res.json({
                                    status: 'success',
                                    msg: 'Manga has been successfully added to the list.',
                                });
                            }).catch(err => console.log(err));
                    });
                }
            }).catch(err => console.log(err));
    }

    // Get Manga Information by ID
    get_manga_by_id = (req, res) => {
        const mangaID = req.params.mangaID;

        Manga.findOne({index: mangaID})
            .then(manga => {
                res.json(manga);
            })
            .catch(err => {
                console.log(err);
                res.json({});
            });
    }

    // Get Manga Pages by ID and chapter
    get_pages = (req, res) => {
        const mangaID = req.params.mangaID;
        const chapterID = req.params.chapter;

        const files = fs.readdirSync(__dirname + '/../storage/manga/' + mangaID + '/' + chapterID + '/');
        const pageCount = files.length;

        Manga.findOne({index: mangaID})
            .select('chapters')
            .then(mangas => {
                let chapters = mangas.chapters;

                let chapter = chapters.filter(chapter => {
                    return chapter.index == chapterID;
                }).pop();

                chapter.page_count = pageCount;

                res.json(chapter);
            })
            .catch(err => {
                console.log(err);
                res.json({});
            });
    } 

    add_view_to_chapter = (req, res) => {
        const mangaID = req.params.mangaID;
        const chapterID = req.params.chapter;

        Manga.findOne({index: mangaID})
        .select('chapters')
        .then(mangas => {
            let chapter_query_index = 0;

            mangas.chapters.every((chapter, index) => {
                if ( chapter.index == chapterID ) {
                    chapter_query_index = index;
                    return false;
                }

                return true;
            });

            if ( mangas.chapters[chapter_query_index].views ) {
                mangas.chapters[chapter_query_index].views += 1;
            }
            else {
                mangas.chapters[chapter_query_index].views = 1;
            }

            mangas.save();

            res.send('Views for ' + chapterID.toString() + ' has been updated to ' + mangas.chapters[chapter_query_index].views.toString() + '.');
        })
        .catch(err => {
            console.log(err);
            res.json({});
        });
    }
}

module.exports = MangaController;