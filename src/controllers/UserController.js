const User = require('../models/User');
const Manga = require('../models/Manga');
const bcrypt = require('bcryptjs');
const passport = require('passport');

class UserController {
    // Get user
    user = (req, res) => {
        console.log(req.user);
        // Send user back
        res.send(req.user);
    }

    // Register
    register = (req, res) => {
        // Set request to it's body
        req = req.body;
        // Validate
        let errs = this.validate_registration(req);
        if ( errs > 0 ) {
            // Send Errors
            res.json({error: errs});
        }
        else {
            // Add a User if it doesn't exist if it does then send error
            User.findOne({ email: req.email })
                .then(user => {
                    if(user) {
                        errs.push('Email is already registered')
                    }

                    return User.findOne({ username: req.username })
                })
                .then(user => {
                    if(user) {
                        errs.push('Username is already used')
                    }

                    if ( errs.length > 0 ) {
                        res.json({error: errs});
                    }
                    else {
                        // Add user here
                        const user = new User({
                            username: req.username,
                            email: req.email,
                            password: req.password
                        });

                        // Hash Password
                        bcrypt.genSalt(10, (err, salt) =>
                            bcrypt.hash(user.password, salt, (err, hash) => {
                                if ( err ) throw err;

                                // Set password to hashed
                                user.password = hash;
                                // Save user
                                user.save()
                                    .then(() => res.json({status: 'success', msg: 'You have been successfully registered. Please proceed login.'}))
                                    .catch(err => console.log(err));
                            }));
                    }
                }).catch(err => console.log(err));
        }
    }

    update_history = (req, res) => {
        const data = req.body;

        // Find User and update it's history
        User.findOne({username: data.username})
            .then(async user => {
                // Updated bool
                let updated = false;
                let message = '';

                // Loop through user's history
                for (let i in user.history) {
                    if ( +user.history[i].mangaID === +data.mangaID ) {
                        user.history[i].chapter = data.chapter;
                        user.history[i].page = data.page;
                        user.history[i].last_updated = Date.now();
                        user.history[i].page = data.page;

                        message = 'History is updated';
                        updated = true;
                    }
                }

                if ( ! updated ) {
                    const historyManga = await Manga.findOne({ index: data.mangaID });

                    user.history.push({
                        mangaID: data.mangaID,
                        chapter: data.chapter,
                        manga_title: historyManga.title,
                        page: data.page,
                        last_updated: Date.now()
                    });

                    message = 'History is generated';
                }

                // Save User
                user.save();

                // Send Response
                res.send(message);
            }).catch(err => console.log(err));
    }

    get_history = (req, res) => {
        const data = req.body;

        User.findOne({ email: data.email })
            .then(user => {
                if ( user ) {
                    user.history.sort((a, b) => new Date(b.last_updated) - new Date(a.last_updated));
                    res.json(user.history);
                }
            }).catch(err => console.log(err));
    }

    get_favorites = (req, res) => {
        const data = req.body;

        User.findOne({ email: data.email })
            .then(user => {
                if ( user ) {
                    // Send List
                    res.json(user.favorites);
                }
            }).catch(err => console.log(err));
    }

    toggle_favorite = (req, res) => {
        const data = req.body;
        let msg = '';

        User.findOne({ email: data.email })
            .then(user => {
                if ( user ) {
                    if ( ! user.favorites ) {
                        user.favorites = [];
                    }

                    if ( user.favorites.includes(+data.manga_index) ) {
                        user.favorites.splice(user.favorites.indexOf(+data.manga_index), 1);
                        msg = 'Manga has been removed from the list.';
                    }
                    else {
                        user.favorites.push(+data.manga_index);
                        msg = 'Manga has been added to the list.';
                    }

                    // Save User
                    user.save();

                    res.send(msg);
                }
            }).catch(err => console.log(err));
    }

    // Login
    login = (req, res, next) => {
        // Authenticate
        passport.authenticate('local', (err, user, message) => {
            if (err) throw err;
            if (!user) res.json({error: ['Username or password is invalid']});
            else {
                req.login(user, err => {
                    if (err) throw err;
                    res.json({status: 'success', msg: 'You have successfully authenticated.', user: req.user});
                });
            }
        })(req, res, next);
    }

    // Logout
    logout = (req, res) => {
        req.logout();
        res.send('successfully logged out');
    }

    // Validate Registration
    validate_registration = (req) => {
        let errs = [];

        if ( req.email === '' || ! req.email ) {
            errs.push('Email field is required');
        }

        if ( req.username === '' || ! req.username ) {
            errs.push('Username field is required');
        }

        if ( req.password === '' || ! req.password ) {
            errs.push('Password field is required');
        }

        if ( req.confirmPassword === '' || ! req.confirmPassword
            || req.confirmPassword !== req.password ) {
            errs.push('Passwords do not match');
        }

        console.log(req);

        if ( req.password.length < 6 ) {
            errs.push('Password should be atleast 6 characters');
        }

        return errs;
    }
}

module.exports = UserController;