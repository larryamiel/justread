const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const download = require('download');
const mongoose = require('mongoose');
const Manga = require('./models/Manga');
const cron = require('node-cron');

// MongoDB
const db = require('./config/database').MongoURI;
mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log('Connected to database (mongoDB)...'))
    .catch(err => console.log(err));

class MangaScraper {
    // Update
    update = async (mangas) => {
        // Update the Manga
        for(let x in mangas) {
            let manga = mangas[x];

            // Log Title
            console.log('----- Updating ' + manga.title + ' -----');

            // Update Manga
            await this.update_manga(manga)
                .then(manga => console.log(manga.title + ' has finished updating'));
        }
    }

    // Update Manga from URL
    update_manga = (manga) => {
        return new Promise(async (resolve, reject) => {
            // Create manga path
            let manga_path = 'storage/manga/' + manga.index;

            if ( ! fs.existsSync(manga_path) ) fs.mkdirSync(manga_path);

            // Update Manga Information
            this.update_database(manga);
            
            // Skip If Chapters Are Completed
            const files = fs.readdirSync('storage/manga/' + manga.index);
            const chapterCount = files.length;

            if ( manga.chapters.length == chapterCount && manga.chapters.length != 0 ) {
                console.log('Skipping ' + manga.title);
                resolve(manga);
            }
            else {
                // Get Chapters
                let chapters = await this.get_chapters(manga);
                
                for(let x in chapters) {
                    let chapter = chapters[x];

                    // Create chapter directory if it doesn't exist if it does then go to next chapter
                    if ( ! fs.existsSync(chapter.path) ) fs.mkdirSync(chapter.path);

                    // If Pages are all downloaded then skip chapter update
                    if ( fs.readdirSync(chapter.path).length > 10 ) console.log('Skipping: ' + manga.title + ' - Chapter ' + chapter.index);
                    else {
                        // Get Pages for Chapter
                        let pages = await this.get_pages(chapter);

                        for(let x in pages) {
                            let page = pages[x];

                            console.log('Downloading: ' + manga.title + ' - Chapter ' + chapter.index + ' page ' + page.index);

                            // Dowload Image on Page
                            this.download_page_image(page, 0);
                        }
                    }
                }

                resolve(manga);
            }
        });
    }

    // Get Chapters
    get_chapters = (manga) => {
        return new Promise((resolve, reject) => {
            if (!manga) reject('Manga not found');

            axios.get(manga.scrape_url)
                .then(res => {
                    const $ = cheerio.load(res.data);
                    
                    let chapters = [];

                    $('#examples .chapter-list a').each(function(index, element) {
                        // Push Chapter Data to Array
                        chapters.push({url: $(this).prop('href')});
                    });

                    // Reverse
                    chapters.reverse();

                    // Add Path to Chapters
                    for(let x in chapters) {
                        let index = +x + 1;
                        let chapter = chapters[x];
                        let chapter_path = 'storage/manga/' + manga.index + '/' + index;

                        chapter.path = chapter_path;
                        chapter.index = index;
                    }

                    resolve(chapters);
                })
                .catch(err => {
                    // Log Error
                    console.log(err)

                    // Reject
                    reject();
                });
        }).catch(() => {
            console.log('Re-Getting Chapters');
            // Redo Pages
            this.get_chapters(manga);
        });
    }

    // Get Pages
    get_pages = (chapter) => {
        return new Promise((resolve, reject) => {
            if (!chapter) reject('Chapter not found');

            axios.get(chapter.url)
                .then(res => {
                    // Initialize Cheerio and Load Site
                    const $ = cheerio.load(res.data);

                    const image_urls = $('#arraydata').html().split(',');
                    let pages = [];
                    let index = 1;

                    for(let x in image_urls) {
                        let image_url = image_urls[x];
                        pages.push({path: chapter.path + '/' + index + '.jpg', url: image_url, index: index});
                        index += 1;
                    }

                    resolve(pages);
                }).catch(err => {
                    // Log Error
                    console.log('get_pages() Error: ' + err);

                    // Reject
                    reject();
                });
        }).catch(() => {
            console.log('Re-Getting Pages');
            // Redo Pages
            this.get_pages(chapter);
        });
    }

    // Download Page Image
    download_page_image = (page, retry) => {
        return new Promise(async (resolve, reject) => {
            if (!page) reject('Page not found');

            try {
                page.url = page.url.replace('amp;', '');

                fs.writeFileSync(page.path, await download(page.url).catch(err => {
                    // Remove File
                    fs.unlink(page.path);

                    // Log
                    console.log('Error! ' + err);

                    // Reject
                    reject();
                }));

                resolve();
            } catch (e) {
                reject();
            }
        }).catch(() => {
            if ( retry < 3 ) {
                retry += 1;
                console.log(retry + ' - Retrying to download page');
                this.download_page_image(page, retry);
            }
            else {
                console.log('------------------- I WILL NOT RETRY ANYMORE - too much man ----------------------')
            }
        });
    }

    // Update Database
    update_database = (manga) => {
        axios.get(manga.scrape_url)
            .then(res => {
                const $ = cheerio.load(res.data);

                // Set Chapters and titles
                let chapters = [];

                $('#examples .chapter-list a').each(function(index, element) {
                    chapters.push({
                        title: $(this).text().trim()
                    })
                });

                chapters.reverse();

                for (let x in chapters) {
                    let chapter = chapters[x];
                    chapter.index = (+x + 1);
                }

                const information = $('.description-update').html().replace(/  /g, '').replace(/\n/g, '').replace(/<br>/g, '*').split('*');

                const author = information[2].replace('<span>Author(s): </span>', '').trim();
                const status = information[7].replace('<span>Status: </span>', '').trim();
                let genres = [];

                cheerio.load(information[4])('a').each(function(index, element) {
                    genres.push($(this).html().trim());
                })

                Manga.findOne({index: manga.index})
                    .then(async (manga) => {
                        // Update Chapters
                        for ( let i in manga.chapters ) {
                            chapters[i].views = manga.chapters[i].views;
                        }
                        
                        manga.chapters = chapters;

                        manga.author = author;
                        manga.status = status;
                        manga.genres = JSON.stringify(genres);
                        manga.last_updated = Date.now();

                        manga.save()
                            .then(() => {
                                console.log(manga.title + ' is successfully updated');
                            }).catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    }
}

const scrape = new MangaScraper();

// Cron Job for Updating Mangas
cron.schedule('0 0 0 * * *', () => {
    Manga.find()
        .then(mangas => {
            scrape.update(mangas);
        });
});

Manga.find()
    .then((mangas) => {
        scrape.update(mangas);
    })
    .catch(err => console.log(err));

module.exports = MangaScraper;