const mongoose = require('mongoose');

const UserHistorySchema = new mongoose.Schema({
    mangaID: Number,
    chapter: Number,
    page: Number,
    manga_title: String,
    last_updated: {
        type: Date,
        default: Date.now()
    }
});

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    history: [UserHistorySchema],
    favorites: [],
    date_created: {
        type: Date,
        default: Date.now
    },
});

const User = mongoose.model('User', UserSchema);
module.exports = User;