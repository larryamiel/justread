const mongoose = require('mongoose');

const MangaChapterSchema = new mongoose.Schema({
    index: Number,
    title: String,
    page_count: Number,
    views: Number,
    date_created: {
        type: Date,
        default: Date.now()
    }
})

const MangaSchema = new mongoose.Schema({
    index: Number,
    title: String,
    scrape_url: String,
    chapters: [MangaChapterSchema],
    description: String,
    genres: String,
    status: String,
    author: String,
    last_updated: Date,
    date_created: {
        type: Date,
        default: Date.now()
    }
})

const Manga = mongoose.model('Manga', MangaSchema);
module.exports = Manga;