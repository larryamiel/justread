const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const serve = require('express-static');
const bodyParser = require('body-parser');
const dotenv = require('dotenv').config();

if (dotenv.error) throw dotenv.error;

const app = express();

// MongoDB
const db = require('./config/database').MongoURI;
mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log('Connected to database (mongoDB)...'))
    .catch(err => console.log(err));

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({
    origin: process.env.CLIENT_IP,
    credentials: true
}));
// Session
app.use(session({
    secret: 'justreadpass',
    resave: true,
    saveUninitialized: true
}));
// Cookies
app.use(cookieParser('justreadpass'));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// Passport Config
require('./config/passport')(passport);

// Routes
app.use('/', require('./routes/index.js'));
app.use('/admin', require('./routes/admin.js'));
app.use('/manga', require('./routes/manga.js'));
app.use('/user', require('./routes/user.js'));

// Storage
app.use('/storage', serve(__dirname + '/storage'));

app.listen(5000, () => {
    console.log('Server is running on port: ' + process.env.HOST + ':' + 5000);
});