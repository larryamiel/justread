const express = require('express');
const router = express.Router();
const MangaController = require('../controllers/MangaController');

const mc = new MangaController();

// Get All Manga
router.post('/all', mc.all);

// Add manga to list
router.post('/add', mc.add);

// Return Manga Information
router.post('/:mangaID', mc.get_manga_by_id);

// Return Chapter Information and Pages
router.post('/:mangaID/:chapter/', mc.get_pages);

// Add View
router.post('/:mangaID/:chapter/view', mc.add_view_to_chapter);

module.exports = router;