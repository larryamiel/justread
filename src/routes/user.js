const express = require('express');
const UserController = require('../controllers/UserController');
const router = express.Router();

const uc = new UserController();

// Index
router.post('/', uc.user);

// Login
router.post('/login', uc.login);

// Logout
router.post('/logout', uc.logout);

// Register
router.post('/register', uc.register);

// Get History
router.post('/history', uc.get_history);

// Update History
router.post('/updateHistory', uc.update_history);

// Get Favorites
router.post('/favorites', uc.get_favorites);

// Get Toggle Favorites
router.post('/toggleFavorite', uc.toggle_favorite);

module.exports = router;